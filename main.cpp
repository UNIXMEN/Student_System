#include <iostream>
#include <fstream>
#include <cstring>


using namespace std;

// clientData structure definition
int ValueStudent;

enum field {
    NAME,
    ID,
    MARK
};

struct Course
{
    string NumberLesson;
    string NameLesson;
    int UnitLesson;
    string NameTeacher;
    int Mark;
    bool Condition;
};

typedef struct Student
{
    string FirstName;
    string LastName;
    string StudentNumber;
    string FileStudents;
    int Average;
    int unitpass;
    struct Student *next;
    struct Course CourseInfo;
} StudentInfo;

typedef struct Lesson
{
    string NumberLesson;
    string NameLesson;
    int UnitLesson;
    string NameTeacher;
    struct Lesson *next;
}LessonInfo;

StudentInfo *headStudent = (StudentInfo *) calloc(1, sizeof(StudentInfo));
LessonInfo *headLesson =(LessonInfo *) calloc(1 ,sizeof(LessonInfo));

unsigned int enterChoice();
void newRecord();
void Add_new_class();
void Add_new_lesson();
int stdcmp(StudentInfo *,StudentInfo *, enum field f);
void stdsort(int len, enum field f);
void Show_Student_Name_Ascending();
void Show_Student_Number_Ascending();
void Show_Student_average_Ascending();
void Show_Student_Rank_Ascending();
void Show_Student_below_Ascending();
void Show_Student_unit_Ascending();
void delete_line_student_info();
void show_student_info();
void delete_course();
int OutputStudents();
int OutputLesson();
int OutputStudentCourse();
int Average(int Switch);

int main()
{
    system("color 05");
    unsigned int choice; // user's choice

    while ((choice = enterChoice()) !=12 ) {
        switch (choice)
        {
            case 1:
                newRecord();
                break;
            case 2:
                Add_new_class();
                break;
            case 3:
                Add_new_lesson();
                break;
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                Average(choice);
                break;
            case 9:
                delete_line_student_info();
                break;
            case 10:
                show_student_info();
                break;
            case 11:
                delete_course();
                break;
            case 12:
                exit(0);
                break;
            default:
                break;

        } // end switch

    } // end while

} // end main

void newRecord()
{
    // create clientData with default information

    string a;
    StudentInfo *p = headStudent;

    cout << "\nEnter FirstName --> ";
    cin >> a;
        p->FirstName = a;
    cout <<"\nEnter LastName --> ";
    cin >> a ;
        p->LastName = a;
    cout << "Please Enter student Number maximum 6 characters --> ";
    while(cin >> a)
    {

        if(a.length()<= 6)
            break;
        else
            cout<<"The student Number is invalid .\n ( The student Number entered should have 6 maximum character ) --> ";
    }
        p->StudentNumber = a;
    // insert record in file
    p->next = NULL;

    OutputStudents();

} // end function newRecord

void Add_new_class()//for student
{
    StudentInfo *p = headStudent;
   // StudentInfo = { "","","","",0,0 };
    string a;
    int mark;

    cout << "Please Enter student Number maximum 6 characters --> ";
    while(cin >> a)
    {

        if(a.length()<= 6)
            break;
        else
            cout<<"The student Number is invalid .\n ( The student Number entered should have 6 maximum character ) --> ";
    }
    p-> StudentNumber = a;
    //here should find the specific student and add a unit
    cout << "Please Enter Lesson number maximum 7 characters you want to added --> ";
    while(cin >> a)
    {

        if(a.length()<= 7)
            break;
        else
            cout<<" The Lesson number is invalid .\n ( The Lesson number entered should have 7 maximum character ) --> ";
    }
    p-> CourseInfo.NumberLesson = a;
    cout << "Enter Student's score --> ";
    cin >> mark;
    p-> CourseInfo.Mark = mark;
    if (p-> CourseInfo.Mark < 10)
    {
        p-> CourseInfo.Condition = false;
    }
    else if (p-> CourseInfo.Mark >= 10)
    {
        p-> CourseInfo.Condition = true;
    }
    p->next = NULL;
    OutputStudentCourse();
    //here should save the score and lesson
}

void Add_new_lesson()//add new lesson to course file
{
    LessonInfo *p = headLesson;
    //first should open the file
    string a;
    int unit= 0;
    cout << "Please Enter Name of lesson maximum 18 characters --> ";
    while(cin >> a)
    {

     if(a.length()<= 18)
         break;
     else
         cout<<"The name of lesson is invalid .\n ( The name of lesson entered should have 18 maximum character ) --> ";

    }
        p-> NameLesson = a;

    cout << "Please Enter Lesson number maximum 7 characters --> ";
    while(cin >> a)
    {

        if(a.length()<= 7)
            break;
        else
            cout<<"The Lesson number is invalid .\n ( The Lesson number entered should have 7 maximum character ) --> ";
    }
        p-> NumberLesson = a;
    cout << "Please Enter Teacher's name maximum 20 characters --> ";
    while(cin >> a)
    {

        if(a.length()<= 20)
            break;
        else
            cout<<"The Teacher's name is invalid .\n ( The Teacher's name entered should have 20 maximum character ) --> ";
    }
        p-> NameTeacher = a;
    cout << "Please Enter Unit of lesson --> ";
    cin >> unit;
    p-> UnitLesson = unit;
    p->next = NULL;
    OutputLesson();
    //then we write it in the file
}

int stdcmp(StudentInfo * x, StudentInfo * y, enum field f)
{

    switch (f) {
        case NAME:
            return x-> FirstName.compare(y-> FirstName);
        case ID:
            return stoi(x-> StudentNumber) - stoi(y-> StudentNumber);
        case MARK:
            return x-> CourseInfo.Mark - y-> CourseInfo.Mark;
        default:
            return x-> CourseInfo.Mark - y-> CourseInfo.Mark;
    }
}

void stdsort(int len, enum field f)
{
    StudentInfo *p = headStudent;
    for (int i = 0; i < len; i++) {
        for (int j = 0; j < len - 1 - i; j++) {
            if (stdcmp(p , p->next , f) > 0) {
                StudentInfo tmp = *p;
                *p = *p->next;
                *p->next = tmp;
            }
        }
    }
}

void Show_Student_Name_Ascending()
{
    StudentInfo *p = headStudent;
    //we can have four array to save all four fields
    //Name		family name			student number			average
    stdsort(ValueStudent, NAME);
    for ( ; ; )
    {
        if (p!= NULL)
        {
            cout<< p-> FileStudents<<"\t"<< p-> LastName<<"\t"<< p->StudentNumber<<"\t"<< p->Average<<endl;
            p = p->next;
        }
        else
            break;
    }

}
void Show_Student_Number_Ascending()
{
    StudentInfo *p = headStudent;
    //we can have four array to save all four fields
    //Name		family name			student number			average
    stdsort(ValueStudent, ID);
    for ( ; ; )
    {
        if (p!= NULL)
        {
            cout<< p-> FileStudents<<"\t"<< p-> LastName<<"\t"<< p->StudentNumber<<"\t"<< p->Average<<endl;
            p = p->next;
        }
        else
            break;
    }
}

void Show_Student_average_Ascending()
{
    StudentInfo *p = headStudent;
    //we can have four array to save all four fields
    //Name		family name			student number			average
    stdsort(ValueStudent, MARK);
    for ( ; ; )
    {
        if (p!= NULL)
        {
            cout<< p-> FileStudents<<"\t"<< p-> LastName<<"\t"<< p->StudentNumber<<"\t"<< p->Average<<endl;
            p = p->next;
        }
        else
            break;
    }

    //we save students with average below 12 into anther structure so we can use in next next function
    //here we save student with below 14 units into anther structure
}
void Show_Student_Rank_Ascending()
{
    StudentInfo *p = headStudent;
    //Name		family name			student number			average
    stdsort(ValueStudent, MARK);

    for ( ; ; )
    {
        if (p!= NULL)
        {
            cout<< p-> FileStudents<<"\t"<< p-> LastName<<"\t"<< p->StudentNumber<<"\t"<< p->Average<<endl;
            p = p->next;
        }
        else
            break;
    }
}
void Show_Student_below_Ascending() //--12
{
    StudentInfo *p = headStudent;
    //Name		family name			student number			average
    for ( ; ; )
    {
        if (p!= NULL)
        {
            if (p-> Average < 12)
            {
                cout<<p->FileStudents<<"\t"<<p->LastName<<"\t"<<p->StudentNumber<<"\t"<<p->Average<<endl;
            }
            p = p->next;
        }
        else
            break;
    }
}
void Show_Student_unit_Ascending() // below 14 units
{
    StudentInfo *p = headStudent;
    //Name		family name			student number			average
    for ( ; ; )
    {
        if (p!= NULL)
        {
            if (p->unitpass < 14)
            {
                cout<<p->FileStudents<<"\t"<<p->LastName<<"\t"<<p->StudentNumber<<"\t"<<p->Average<<endl;
            }
            p = p->next;
        }
        else
            break;
    }
}
void delete_line_student_info()
{
    fstream Students;
    ofstream outfile;
    string data,input;
    string line, deleteMovie;
    Students.open("students.txt", ios::in);
    cin >> input;
    if (!Students.is_open())
    {
        perror("fopen");
        return;
    }
    outfile.open("new.txt");

    while (getline(Students, line))
    {
        string token_number;
        size_t pos=0;
        pos = line.find(",");
        token_number=line.substr(0, pos);

        if (stoi(token_number) != stoi(input))
            outfile << line << endl;
    }
    outfile.close();
    Students.close();

    string append;
    fstream Course;
    append.append(input);
    append.append(".txt");
    remove(append.c_str());
    remove("students.txt");
    rename("new.txt", "students.txt");
}
void delete_course()
{
    fstream Students;
    ofstream outfile;
    string data, input;
    string line, deleteMovie;
    Students.open("courses.txt", ios::in);
    cin >> input;
    if (!Students.is_open())
    {
        perror("fopen");
        return;
    }
    outfile.open("new.txt");

    while (getline(Students, line))
    {
        string token_number;
        size_t pos = 0;
        pos = line.find(",");
        token_number = line.substr(0, pos);

        if (stoi(token_number) != stoi(input))
            outfile << line << endl;
    }
    outfile.close();
    Students.close();
    remove("courses.txt");
    rename("new.txt", "courses.txt");
}
void show_student_info()
{
    fstream Students;
    ofstream outfile;
    string input;
    string line;
    bool flag = false;
    Students.open("students.txt", ios::in);
    if (!Students.is_open())
    {
        perror("fopen");
        return;
    }
    cin >> input;
    while (getline(Students, line))
    {
        string token_number,token_fname,token_lname;
        size_t pos = 0;
        pos = line.find(",");
        token_number = line.substr(0, pos);
        if (stoi(token_number) == stoi(input))
        {
            pos = line.find(ios_base::cur);
            token_fname = line.substr(0, pos);
            pos = line.find(ios_base::cur);
            token_lname = line.substr(0, pos);
            cout << token_fname << "\t" << token_lname<<"\t" << token_number<<endl;
            flag = true;
            break;
        }

    }
    if (flag==false)
    {
        cout << "There is no Student with this Accout number ! "<<endl;
    }
}
unsigned int enterChoice()
{
    unsigned int menuChoice;

    for(int g=0 ;g <26000;g++)
    {
        for(int h=0; h<28000 ;h++)
        {
            h++;
        }
    }

    system("clear");
    cout << "1 -To Add Student " << endl << "2 -To Add New Course for Student " << endl << "3 -To Add New Course " << endl <<
         "4 -To Display Student's Names In Ascending Mode " << endl << "5 -To Display Student's Numbers in Ascending Mode " << endl <<
         "6 -To Display Probations Students " << endl << "7 -To Display Ranks "<<endl << "8 -To Show Students with below 14 Courses " << endl <<
         "9 -To Delete Specific Students " << endl << "10 -To Display information about Specific Students " << endl << "11 -To Delete Specific Courses" << endl << "12 -Exit"<<endl;
    cin >> menuChoice;
    return menuChoice;
} // end function enterChoice7

int OutputStudents()
{
    StudentInfo *p = headStudent;
    fstream student;
    string data;
    student.open("students.txt", ios::app |ios::in);

    if (!student.is_open())
    {
        perror("fopen");
        return 0;
    }
    while( student >> data)
    {
        const char s[2] = ",";
        char *token;
        char *y = new char[data.length() + 1];
        strcpy(y, data.c_str());

        token = strtok(y, s);
        if (token == p->StudentNumber)
        {
            cout<<" \n!!  The Student number existed  !!  Please enter another Student number. \n";

            for(int g=0 ;g <26000;g++)
            {
                for(int h=0; h<28000 ;h++)
                {
                    h++;
                }
            }

            system("clear");
            newRecord();
        }
        delete []y;
    }
    student.clear();  //for clear Students file like function rewind in file c
    student.seekg(0);  //for clear Students file like function rewind in file c

    student << p->StudentNumber << "," << p->FirstName << "," << p->LastName << "," << p->StudentNumber << ".txt" << endl;

    p->next = NULL;
    cout<<"\n!! The requested information was added !!\n";
    student.close();

}

//function for save information courses in file courses.txt
int OutputLesson()
{
    LessonInfo *p = headLesson;
    string data;

    fstream lesson;
    lesson.open("courses.txt", ios::app | ios::in);

    if (!lesson.is_open())
    {
        perror("fopen");
        return 0;
    }
    while( lesson >> data)
    {
        const char s[2] = ",";
        char *token;
        char *y = new char[data.length() + 1];
        strcpy(y, data.c_str());

        token = strtok(y, s);
        if (token == p->NumberLesson)
        {
            cout<<" \n!!  The lesson number existed  !!  Please enter another lesson number. \n";

            for(int g=0 ;g <26000;g++)
            {
                for(int h=0; h<28000 ;h++)
                {
                    h++;
                }
            }

            system("clear");
            Add_new_lesson();
        }
        delete []y;
    }
    lesson.clear();  //for clear Students file like function rewind in file c
    lesson.seekg(0);  //for clear Students file like function rewind in file c

    lesson << p-> NumberLesson << "," << p-> NameLesson << "," << p-> UnitLesson << "," << p-> NameTeacher << endl;
    cout<<"\n!! The requested information was added !!\n";
    p->next = NULL;
    lesson.close();
}
//function for save information courses of student in file NumberStudent.txt
int OutputStudentCourse()
{
    StudentInfo *p = headStudent;

    string data;
    const char s[2] = ",";
    int h = 0;
    fstream Lesson;
    Lesson.open("courses.txt", ios::in);
    if (!Lesson.is_open())
    {
        perror("fopen");
        return 0;
    }
    while (Lesson >> data && h != 1)
    {
        const char *token;
        char *y = new char[data.length() + 1];
        strcpy(y, data.c_str());

        token = strtok(y, s);
        if (token == p-> CourseInfo.NumberLesson)
        {
            token = strtok(NULL, s);
            p-> CourseInfo.NameLesson = token;
            token = strtok(NULL, s);
            sscanf(token, "%d", & p->CourseInfo.UnitLesson);
            token = strtok(NULL, s);
            p-> CourseInfo.NameTeacher = token;
            h = 1;
        }

    }
    if (h == 0)
    {
        cout <<"The lesson doesn't exist !";
        Add_new_class();
    }
    string append;
    char address[11];
    fstream Course;
    append.append(p -> StudentNumber);
    append.append(".txt");
    strcpy(address, append.c_str());

    Course.open(address, ios::app);

    if (!Course.is_open())
    {
        perror("fopen");
        return 0;
    }

    Course << p-> CourseInfo.NumberLesson << "," << p-> CourseInfo.NameLesson << "," << p ->CourseInfo.UnitLesson << "," << p-> CourseInfo.NameTeacher << "," << p-> CourseInfo.Mark << "," << p-> CourseInfo.Condition << endl;
    cout<<"\n!! The requested information was added !!\n";
    p->next = NULL;

    Lesson.close();
    Course.close();
}

int Average(int Switch)
{
    StudentInfo *p = headStudent;

    string append;
    string data;
    char address[11];
    int j = 0;

    fstream Students;
    Students.open("students.txt", ios::in);

    if (!Students.is_open())
    {
        perror("fopen");
        return 0;
    }

    while (Students >> data)
    {
        j++;
    }
    ValueStudent = j;

    Students.clear();  //for clear Students file like function rewind in file c
    Students.seekg(0);  //for clear Students file like function rewind in file c

    while (Students >> data )
    {
        ///////////////////////////
        const char s[2] = ",";
        char *token;
        char *y = new char[data.length() + 1];
        strcpy(y, data.c_str());

        token = strtok(y, s);
        p-> StudentNumber = token;

        token = strtok(NULL, s);
        p-> FirstName = token;

        token = strtok(NULL, s);
        p-> LastName = token;

        token = strtok(NULL, s);
        p-> FileStudents = token;

        /////////////////////////////
        fstream course;
        int summark = 0, sumunit = 0, sumunitpass = 0;
        strcpy(address, p-> FileStudents.c_str());
        course.open(address, ios::in);

        if (!course.is_open())
        {
            perror("fopen");
            return 0;
        }

        while (course >> data)
        {
            char *x = new char[data.length() + 1];
            strcpy(x, data.c_str());
            token = strtok(x, s);
            p-> CourseInfo.NumberLesson = token;
            token = strtok(NULL, s);
            p-> CourseInfo.NameLesson = token;
            token = strtok(NULL, s);
            sscanf(token, "%d", &p-> CourseInfo.UnitLesson);
            token = strtok(NULL, s);
            p-> CourseInfo.NameTeacher = token;
            token = strtok(NULL, s);
            sscanf(token, "%d", &p-> CourseInfo.Mark);
            token = strtok(NULL, s);
            sscanf(token, "%d", &p-> CourseInfo.Condition);
            summark += p-> CourseInfo.Mark * p-> CourseInfo.UnitLesson;
            sumunit += p-> CourseInfo.UnitLesson;
            if (p-> CourseInfo.Condition == 1)
            {
                p-> unitpass += p-> CourseInfo.UnitLesson;
            }
            delete []x;
        }
        p-> Average = summark / sumunit;
        course.close();
        p-> next = (StudentInfo *) calloc (1, sizeof(StudentInfo));
        p = p->next;
        delete []y;
    }
    p = NULL;
    Students.close();
    switch (Switch)
        {
            case 4:
                Show_Student_Name_Ascending();
                break;
            case 5:
                Show_Student_Number_Ascending();
                break;
            case 6:
                Show_Student_below_Ascending();
                break;
            case 7:
                Show_Student_Rank_Ascending();
                break;
            case 8:
                Show_Student_unit_Ascending();
                break;
        }
}
