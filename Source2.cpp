#include <iostream>
#include <string>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <cstdio>
#include <cstring>
#include <stdlib.h>
#include <cstdlib>

using namespace std;

//string getFileContents(ifstream& File);
// clientData structure definition
int ValueStudent;

typedef struct student {
	char name[100];
	int id;
	double mark;
} std_t;
enum field {
	NAME,
	ID,
	MARK
};

struct Course
{
	string NumberLesson;
	char NameLesson[18];
	//string NameLesson[18];
	int UnitLesson;
	char NameTeacher[20];
	//string NameTeacher[20];
	int Mark;
	bool Condition;
};

struct Student
{
	string FirstName;
	string LastName;
	int StudentNumber;
	string FileStudents;
	struct Course CourseInfo;
}StudentInfo;
struct Lesson
{
	string NumberLesson;
	char NameLesson[18];
	//string NameLesson;
	int UnitLesson;
	char NameTeacher[20];
	//string NameTeacher;
}LessonInfo;

struct clientData {
	unsigned int acctNum; // account number
	char lastName[15]; // account last name
	char firstName[10]; // account first name
	double balance; // account balance
};
unsigned int enterChoice(void);
void textFile(FILE *readPtr);
void updateRecord(FILE *fPtr);
void newRecord();
void deleteRecord(FILE *fPtr);
void Add_new_class();
void Add_new_lesson();
int stdcmp(std_t x, std_t y, enum field f);
void stdsort(std_t a[], int len, enum field f);
void Show_Student_Name_Ascending();
void Show_Student_Number_Ascending();
void Show_Student_average_Ascending();
void Show_Student_Rank_Ascending();
void Show_Student_below_Ascending();
void Show_Student_unit_Ascending();
int OutputStudents();
int OutputLesson();
int OutputStudentCourse();
int Average(int Switch);

int main(void)
{
	system("color 05");
	///FILE *cfPtr; // credit.dat file pointer
	unsigned int choice; // user's choice

		// fopen opens the file; exits if file cannot be opened

	///if ((cfPtr = fopen("credit.dat", "rb+")) == NULL) {
	///	puts("File could not be opened.");
	///} // end if
///	else {
		// enable user to specify action
//	cin >> choice;
		while ((choice = enterChoice()) !=12 ) {
			switch(choice)
			{
				case 1:
					newRecord();
				break;
			case 2:
				Add_new_class();
				break;
			case 3:
				Add_new_lesson();
				break;
			case 4:
				Show_Student_Name_Ascending();
				break;
			case 5:
				Show_Student_Number_Ascending();
				break;
			case 6:
				Show_Student_below_Ascending();
				break;
			case 7:
				Show_Student_Rank_Ascending();
				break;
			case 8:
				Show_Student_unit_Ascending();
				break;
			case 9:
				break;
			case 10:
				
				break;
			case 11:
				break;
			case 12:
				exit(0);
				break;
			default:
				break;

			} // end switch

	///	} // end while

		///fclose(cfPtr); // fclose closes the file

	} // end else
} // end main
// create formatted text file for printing
void textFile(FILE *readPtr)
{
	FILE *writePtr; // accounts.txt file pointer
	int result; //
	struct clientData client = { 0, "", "", 0.0 };

	// fopen opens the file; exits if file cannot be opened
	if ((writePtr = fopen("accounts.txt", "w")) == NULL) {
		puts("File could not be opened.");

	} // end if
	else {
		rewind(readPtr); // sets pointer to beginning of file
		fprintf(writePtr, "%-6s%-16s%-11s%10s\n",
			"Acct", "Last Name", "First Name", "Balance");
		// copy all records from random-access file into text file
		while (!feof(readPtr)) {
			result = fread(&client, sizeof(struct clientData), 1, readPtr);

			// write single record to text file
			if (result != 0 && client.acctNum != 0) {
				fprintf(writePtr, "%-6d%-16s%-11s%10.2f\n",
					client.acctNum, client.lastName,
					client.firstName, client.balance);
			} // end if

		} // end while

		fclose(writePtr); // fclose closes the file
	} // end else

} // end function textFile
// update balance in record
void updateRecord(FILE *fPtr)
{
	unsigned int account; // account number
	double transaction; // transaction amount

		// create clientData with no information
	struct clientData client = { 0, "", "", 0.0 };

	// obtain number of account to update

	cout << "Enter account to update ( 1 - 100 ): " << endl;
	cin >> account;
	fseek(fPtr, (account - 1) * sizeof(struct clientData), SEEK_SET);
	// read record from file
   // display error if account does not exist
	fread(&client, sizeof(struct clientData), 1, fPtr);
	if (client.acctNum == 0) {
		cout << "Account #%d has no information." << endl;
	} // end if
	else { // update record
		printf("%-6d%-16s%-11s%10.2f\n\n",
			client.acctNum, client.lastName,
			client.firstName, client.balance);

		// request transaction amount from user
		printf("%s", "Enter charge ( + ) or payment ( - ): ");
		scanf("%lf", &transaction);
		client.balance += transaction; // update record balance

		printf("%-6d%-16s%-11s%10.2f\n",
			client.acctNum, client.lastName,
			client.firstName, client.balance);

		fseek(fPtr, (account - 1) * sizeof(struct clientData),
			SEEK_SET);
		// write updated record over old record in file
		fwrite(&client, sizeof(struct clientData), 1, fPtr);
	} // end else

} // end function updateRecord

void deleteRecord(FILE *fPtr)
{
	struct clientData client; // stores record read from file
	struct clientData blankClient = { 0, "", "", 0 }; // blank client

	unsigned int accountNum; // account number
// obtain number of account to delete
	printf("%s", "Enter account number to delete ( 1 - 100 ): ");
	scanf("%d", &accountNum);
	fseek(fPtr, (accountNum - 1) * sizeof(struct clientData),
		SEEK_SET);
	// read record from file
	fread(&client, sizeof(struct clientData), 1, fPtr);
	// display error if record does not exist
	if (client.acctNum == 0) {
		printf("Account %d does not exist.\n", accountNum);

	} // end if
	else {
		fseek(fPtr, (accountNum - 1) * sizeof(struct clientData), SEEK_SET);
		fwrite(&blankClient, sizeof(struct clientData), 1, fPtr);
	}
}
void newRecord()
{
	// create clientData with default information
	StudentInfo = { "","",0,"" };
	unsigned int accountNum; // account number

	// obtain number of account to create
	
	//cout << "Enter new Student Number : " << endl;
	//cin >> StudentInfo.StudentNumber;

	///fseek(fPtr, (accountNum - 1) * sizeof(struct clientData),SEEK_SET);
	// read record from file
	///fread(&client, sizeof(struct clientData), 1, fPtr);
	// display error if account already exists
	//if (StudentInfo.acctNum != 0) {
		///printf("Account #%d already contains information.\n",
		///	StudentInfo.acctNum);

	///} // end if
	///else { // create record
	cout << "Enter FirstName, Lastname, Student Number, Code of Lesson,Student's Mark : " << endl;
	cin >> StudentInfo.FirstName >> StudentInfo.LastName>>StudentInfo.StudentNumber >> StudentInfo.CourseInfo.NumberLesson >> StudentInfo.CourseInfo.Mark;
	if (StudentInfo.CourseInfo.Mark < 10) 
	{
		StudentInfo.CourseInfo.Condition = false;
	}
	else if (StudentInfo.CourseInfo.Mark >= 10)
	{
		StudentInfo.CourseInfo.Mark = true;
	}
	OutputStudents();
	///	client.acctNum = accountNum;
	///	fseek(fPtr, (StudentInfo.acctNum - 1) *sizeof(struct clientData), SEEK_SET);
		// insert record in file
	///	fwrite(&client,sizeof(struct clientData), 1, fPtr);
	///} // end else
} // end function newRecord

void Add_new_class()//for student
{
	StudentInfo = { "","",0,"" };
	cout << "Entet student Number : ";
	cin >> StudentInfo.StudentNumber;
	//here should find the specific student and add a unit
	cout << "Enter the code of the lesson you want to add : ";
	cin >> StudentInfo.CourseInfo.NumberLesson;
	cout << "Enter Student's score : ";
	cin >> StudentInfo.CourseInfo.Mark;
	OutputStudentCourse();
	//here should save the score and lesson
}

void Add_new_lesson()//add new lesson to course file 
{
	struct Course CourseInfo = { "","",0,"",0,NULL };
	//first should open the file
	string Name_lesson, code_lesson, Teacher_name;
	int unit;
	cout << "Please Enter Name of less : ";
	cin >> CourseInfo.NameLesson;
	cout << "Please Enter Lesson code : ";
	cin >> CourseInfo.NumberLesson;
	cout << "Please Enter Teacher's name : ";
	cin >> CourseInfo.NameTeacher;
	cout << "Please Enter Unit";
	cin >> CourseInfo.UnitLesson;
	OutputLesson();
	//then we write it in the file
}

int stdcmp(std_t x, std_t y, enum field f)
{
	switch (f) {
	case NAME:
		return strcmp(x.name, y.name);
	case ID:
		return x.id - y.id;
	case MARK:
		return x.mark - y.mark;
	default:
		return x.id - y.id;
	}
}

void stdsort(std_t a[], int len, enum field f)
{
	for (int i = 0; i < len; i++) {
		for (int j = 0; j < len - 1 - i; j++) {
			if (stdcmp(a[j], a[j + 1], f) > 0) {
				struct student tmp = a[j];
				a[j] = a[j + 1];
				a[j + 1] = tmp;
			}
		}
	}
}

void Show_Student_Name_Ascending()
{
	//int number = getlines(path to the student file);
	//we can have four array to save all four fields
	string arr[100];
	//Name		family name			student number			average	
	std_t a[3] = { { "hossein", 1, 10 },{ "ali", 3, 11 },{ "hassan", 2, 12 } };
	stdsort(a, 3, NAME);
	for (int i = 0; i < 3; i++) {
		printf("\na[%d].name = %s", i, a[i].name);
		printf("\na[%d].id = %d", i, a[i].id);
		printf("\na[%d].mark = %lf", i, a[i].mark);
		printf("\n");
	}
	while (!feof)
	{
		int i = 0;
		char line[100];
		//fgets(line,100,File *ptr)
		arr[i] = strtok(line, " ");
	}

}
void Show_Student_Number_Ascending()
{
	//int number = getlines(path to the student file);
	//we can have four array to save all four fields
	int arr[100];
	//Name		family name			student number			average	
	std_t a[3] = { { "hossein", 1, 10 },{ "ali", 3, 11 },{ "hassan", 2, 12 } };
	stdsort(a, 3, ID);
	for (int i = 0; i < 3; i++) {
		printf("\na[%d].name = %s", i, a[i].name);
		printf("\na[%d].id = %d", i, a[i].id);
		printf("\na[%d].mark = %lf", i, a[i].mark);
		printf("\n");
	}
	/*while (!feof)
	{
		int i = 0;
		char line[100];
		//fgets(line,100,File *ptr)
		arr[i] = strtok(line, " ");
	}*/

}

void Show_Student_average_Ascending()
{
	//int number = getlines(path to the student file);
	//we can have four array to save all four fields
	double arr[100];
	//Name		family name			student number			average	
	std_t a[3] = { { "hossein", 1, 10 },{ "ali", 3, 11 },{ "hassan", 2, 12 } };
	stdsort(a, 3, MARK);
	for (int i = 0; i < 3; i++) {
		printf("\na[%d].name = %s", i, a[i].name);
		printf("\na[%d].id = %d", i, a[i].id);
		printf("\na[%d].mark = %lf", i, a[i].mark);
		printf("\n");
	}
	/*while (!feof)
	{
		int i = 0;
		char line[100];
		//fgets(line,100,File *ptr)
		arr[i] = strtok(line, " ");
	}*/

	//we save students with average below 12 into anther structure so we can use in next next function
	//here we save student with below 14 units into anther structure
}
void Show_Student_Rank_Ascending()
{
	//Name		family name			student number			average	
}
void Show_Student_below_Ascending() //--12
{
	//Name		family name			student number			average	
}
void Show_Student_unit_Ascending() // below 14 units
{
	//Name		family name			student number			average	
}

unsigned int enterChoice(void)
{
	unsigned int menuChoice;
	cout << "1 -To Add Student " << endl << "2 -To Add New Course for Student " << endl << "3 -To Add New Course " << endl <<
		"4 -To Display Student's Names In Ascending Mode " << endl << "5 -To Display Student's Numbers in Ascending Mode " << endl <<
		"6 -To Display Probations Students " << endl << "7 -To Display Ranks " << "8 -To Show Students with below 14 Courses " << endl <<
		"9 - To Delete Specific Students " << endl << "10 - To Display information about Specific Students " << endl <<"11 -To Delete Specific Courses" << endl<<"12 -Exit";
	cin >> menuChoice;
	return menuChoice;
} // end function enterChoice7

int OutputStudents()
{
	fstream student;
	student.open("students.txt", ios::app);

	if (!student.is_open())
	{
		perror("fopen");
		return 0;
	}
	student << StudentInfo.StudentNumber << "," << StudentInfo.FirstName << "," << StudentInfo.LastName << "," << StudentInfo.StudentNumber << ".txt" << endl;

	student.close();

	/*while (fgets(infostudent, 150, student) != NULL)
	{
	const char s[2] = ",";
	char *token;

	token = strtok(infostudent, s);
	strcpy(StudentInfo.FirstName,token);
	token = strtok(NULL, s);
	strcpy(StudentInfo.LastName,token);
	token = strtok(NULL, s);
	strcpy(StudentInfo.StudentNumber,token);

	if (StudentInfo.StudentNumber != StudentNumber)
	continue;

	token = strtok(NULL, s);
	strcpy(StudentInfo.CourseInfo.NumberLesson,token);

	if (StudentInfo.CourseInfo.NumberLesson,token != CourseNumber)
	continue ;

	token = strtok(NULL, s);
	sscanf(token, "%d",  &StudentInfo.CourseInfo.UnitLesson);
	}
	*/
}


//function for save information courses in file courses.txt
int OutputLesson()
{
	fstream lesson;
	lesson.open("courses.txt", ios::app);

	if (!lesson.is_open())
	{
		perror("fopen");
		return 0;
	}

	lesson << LessonInfo.NumberLesson << "," << LessonInfo.NameLesson << "," << LessonInfo.UnitLesson << "," << LessonInfo.NameTeacher << endl;

	lesson.close();
}
//function for save information courses of student in file NumberStudent.txt
int OutputStudentCourse()
{
	fstream Course;
	string append;
	//append.append(StudentInfo.StudentNumber);
	append.append(".txt");
	Course.open(append, ios::app);

	if (!Course.is_open())
	{
		perror("fopen");
		return 0;
	}

	Course << StudentInfo.CourseInfo.NumberLesson << "," << StudentInfo.CourseInfo.NameLesson << "," << StudentInfo.CourseInfo.UnitLesson << "," << StudentInfo.CourseInfo.NameTeacher << "," << StudentInfo.CourseInfo.Mark << "," << StudentInfo.CourseInfo.Condition << endl;

	Course.close();
}

int Average(int Switch)
{
	string append;
	string data;
	char address[11];
	int j = 0;



	fstream Students;
	Students.open("students.txt", ios::in);

	if (!Students.is_open())
	{
		perror("fopen");
		return 0;
	}

	while (Students >> data)
	{
		j++;
	}
	ValueStudent = j;

	struct Student *p;
	p = (struct Student *)calloc((size_t)j, sizeof(struct Student));

	int i = 0;
	while ((Students >> data) != NULL)
	{

		///////////////////////////
		const char s[2] = ",";
		char *token;
		char *y = new char[data.length() + 1];
		strcpy(y, data.c_str());

		token = strtok(y, s);
		p[i].StudentNumber = token;

		token = strtok(NULL, s);
		p[i].FirstName = token;

		token = strtok(NULL, s);
		p[i].LastName = token;

		token = strtok(NULL, s);
		p[i].FileStudents = token;

		/////////////////////////////
		fstream course;
		int summark = 0, sumunit = 0;
		strcpy(address, p[i].FileStudents.c_str());
		Students.open(address, ios::in);
		while ((Students >> data) != NULL)
		{
			char *x = new char[data.length() + 1];
			token = strtok(x, s);
			p[i].CourseInfo.NumberLesson = token;
			token = strtok(NULL, s);
			p[i].CourseInfo.NameLesson = token;
			token = strtok(NULL, s);
			sscanf(token, "%d", &p[0].CourseInfo.UnitLesson);
			token = strtok(NULL, s);
			p[i].CourseInfo.NameTeacher = token;
			token = strtok(NULL, s);
			sscanf(token, "%d", &p[i].CourseInfo.Mark);
			summark += p[i].CourseInfo.Mark * p[0].CourseInfo.UnitLesson;
			sumunit += p[i].CourseInfo.UnitLesson;
		}
		p[i].Average = summark / sumunit;
		course.close();
		i++;
	}
	free(p);
}



/*
int main()
{
	//system("color 0B");
	system("color 05");
	int x = 0;
	cin >> x;
	switch (x)
	{
	case 1:
		Add_Student();
		break;
	case 2:
		break;
	case 3:
		break;
	case 4:
		break;
	case 5:
		break;
	case 6:
		break;
	case 7:
		break;
	case 8:
		break;
	case 9:
		break;
	case 10:
		break;
	case 11:
		break;
	case 12:
		exit(0);
		break;
	default:
		break;
	}
	system("pause");

ifstream Reader("D:\New Text Document.txt");             //Open file

	string Art = getFileContents(Reader);       //Get file

	cout << Art << std::endl;               //Print it to the screen

	Reader.close();                           //Close file
	system("pause");
}
void Add_Student()
{
	string Name;
	string Family_Name;
	int Student_No;

}
*/
/*string getFileContents(ifstream& File)
{
	string Lines = "";        //All lines

	if (File)                      //Check if everything is good
	{
		while (File.good())
		{
			string TempLine;                  //Temp line
			getline(File, TempLine);        //Get temp line
			TempLine += "\n";                      //Add newline character

			Lines += TempLine;                     //Add newline
		}
		return Lines;
	}
	else                           //Return error
	{
		return "ERROR File does not exist.";
	}
}
*/